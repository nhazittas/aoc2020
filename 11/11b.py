import sys
import copy
lines = [[ c for c in line.strip()] for line in sys.stdin]

m = len(lines)
n = len(lines[0])
print(m,n)


def check_sight(hood, x, y, dx, dy):
    #print('cs', x,y, dx, dy)
    row = x + dx
    col = y + dy
    while True:
        if row < 0 or row >= m or col < 0 or col >= n:
            return 0
        if hood[row][col] != '.':
            break
        row += dx
        col += dy
    if hood[row][col] == '#':
        return 1
    return 0

def occupied_neighbors(hood, x, y):
    occupied = 0
    for dx in range(-1,2):
        for dy in range(-1,2):
            if dx == 0 and dy == 0:
                continue
            occupied += check_sight(hood, x, y, dx, dy)
    return occupied

def print_2d(seats):
    print('-'*(n+2))
    for row in seats:
        print('|', end='')
        for c in row:
            print(c, end='')
        print('|')
    print('-'*(n+2))


def next_gen(seats):
    next_gen = copy.deepcopy(seats)
    for row in range(m):
        for col in range(n):
            og = seats[row][col]
            if og == '.':
                continue
            else:
                neighbors = occupied_neighbors(seats, row, col)
                if og == 'L':
                    if neighbors == 0:
                        next_gen[row][col] = '#'
                elif og == '#':
                    if neighbors >= 5:
                        next_gen[row][col] = 'L'
                else:
                    print('unknown char', og, "at", row, col)
    return next_gen

def compare_seats( s1, s2):
    for row in range(m):
        for col in range(n):
            if s1[row][col] != s2[row][col]:
                return False
    return True

#print_2d(lines)
last_gen = lines
current_gen = next_gen(last_gen)

while not compare_seats(last_gen, current_gen):
    #print_2d(current_gen)
    last_gen = current_gen
    current_gen = next_gen(current_gen)

#print_2d(current_gen)
num_hash = sum([sum([1 if c == '#' else 0 for c in row]) for row in current_gen])
print(num_hash)