import sys
import copy
import math
import re
from collections import defaultdict
from functools import lru_cache
lines = [line.strip() for line in sys.stdin]

s = set()

known_contaminants = dict()
known_contaminated_ingredent = dict()

ingredent_list = []
contaminant_list = []
ingredient_in = defaultdict(set)
contaminant_in = defaultdict(set)

i=0
for line in lines:
    ingredents, contaminants = line.split("(")
    ingredents = ingredents.strip().split()
#":a".strip()
    contaminants =  [c.strip("), ") for c in contaminants.split()[1:]]
    print(ingredents, contaminants)

    ingredent_list.append(set(ingredents))
    for ingredent in ingredents:
        ingredient_in[ingredent].add(i)

    contaminant_list.append(set(contaminants))
    for contaminant in contaminants:
        contaminant_in[contaminant].add(i)

    i+=1

print("ci", contaminant_in)

while len(known_contaminants) < len(contaminant_in):
    rn = 0
    for contaminants in contaminant_list:
        unkown_contaminants = contaminants - set(known_contaminants)
        possibily_clean_ingredents = ingredent_list[rn] - set(known_contaminated_ingredent)

        if len(unkown_contaminants) == 1 and len(possibily_clean_ingredents) == 1:
            fount_contaminant = list(unkown_contaminants)[0]
            dirty_ingredient = list(possibily_clean_ingredents)[0]

            known_contaminants[fount_contaminant] = dirty_ingredient
            known_contaminated_ingredent[dirty_ingredient] = fount_contaminant

        rn+=1

    # loop over all pairs of recipies

    for contaminant in contaminant_in:
        if contaminant in set(known_contaminants):
            continue

        recipie_set = contaminant_in[contaminant]

        print(ingredent_list, recipie_set)
        possible_ingridients = ingredent_list[list(recipie_set)[0]].copy()
        for recipie_num in recipie_set:
            ingredient_set = ingredent_list[recipie_num] - set(known_contaminated_ingredent)

            possible_ingridients = possible_ingridients.intersection(ingredient_set)
            print("------------ C< PI",contaminant, possible_ingridients)
        if len(possible_ingridients) == 1:
            dirty_ingredient = list(possible_ingridients)[0]
            fount_contaminant = contaminant

            known_contaminants[fount_contaminant] = dirty_ingredient
            known_contaminated_ingredent[dirty_ingredient] = fount_contaminant

    print("******** KC:",known_contaminants)
print(known_contaminants)

clean_ingredients = set(ingredient_in) - set(known_contaminated_ingredent)

print(clean_ingredients)

sum_clean_times = 0
for ingredents in ingredent_list:
    clean_matches = ingredents.intersection(clean_ingredients)
    sum_clean_times += len(clean_matches)

print(sum_clean_times)

sorted_contaminants = sorted(set(known_contaminants))

sorted_ingredients = [known_contaminants[c] for c in sorted_contaminants]
print(sorted_ingredients)
# bqkndvb,zmb,bmrmhm,snhrpv,vflms,bqtvr,qzkjrtl,rkkrx
