import sys
import copy
import math
import re
from collections import defaultdict
from functools import lru_cache
lines = [line.strip() for line in sys.stdin]

tile_num = 0
#"".find()
current_tile = []
tiles = dict()
tile_side_ids = dict()
tile_matches = defaultdict(list)
for line in lines:
    if line.find("Tile") == 0:
        
        tile_num = int(line.strip("Tile :"))
        #print("TN =", tile_num)
    elif len(line) == 0:
        if len(current_tile) > 0:
           tiles[tile_num] = current_tile
           #print("Save", tile_num)
           current_tile = []
        continue
    else:
        #print(line)
        current_tile.append(line)
        #print(current_tile)

def convert_to_id(side):
    id = 0
    for c in side:
        id *= 2
        if c == "#":
            id +=1

    return id

def get_side_ids(tile):
    normal = []
    r = []

    normal.append(list(tile[0]))
    normal.append(list(tile[-1]))
    normal.append([row[0] for row in tile])
    normal.append([row[-1] for row in tile])

    for n in normal:
        rev = n.copy()
        rev.reverse()
        r.append(rev)
    #print(n, r)
    return normal, r

for tID in tiles:
    tile =tiles[tID]

    n, r = get_side_ids(tile)
    nids = [convert_to_id(nn) for nn in n]
    rids = [convert_to_id(rr) for rr in r]

    #print(tID, set(nids), set(rids), set(nids+rids))
    tile_side_ids[tID] = (set(nids), set(rids), set(nids + rids))

corners = []
for tID in tiles:
    tile =tiles[tID]
    tile_side_id = tile_side_ids[tID]
    sides_wo_match = 0
    sides_w_match = 0
    for sID in tile_side_id[2]:
        found = 0
        for other_tID in tiles:
            if other_tID == tID:
                continue
            other_tile_sides = tile_side_ids[other_tID]
            if sID in other_tile_sides[0]:
                tile_matches[tID].append(other_tID)
                #tile_matches[other_tID].append(tID)
                found += 1 
                break
        if found == 0:
            sides_wo_match += 1 
        else:
            sides_w_match += 1
        #print("f", found)

    #print(tID, "swo", sides_wo_match, "sw", sides_w_match)
    print(sides_w_match)
    if sides_w_match == 2:
        #print(tID)
        corners.append(tID)
p=1
for c in corners:
    p*=c
    print(c)
print(p)

print(tile_matches)

                


