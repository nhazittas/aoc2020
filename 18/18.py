import sys
import copy
import math
import re
from collections import defaultdict
from functools import lru_cache
lines = [line.strip() for line in sys.stdin]

mode = 0

rules = dict()
messages = []
for line in lines:
    #print(line, len(line))
    if len(line) == 0:
        print("MODE CHANGE LINE")
        mode +=1
        continue
    elif mode == 0:
        line = line.split(":")
        rule = int(line[0])
        value = line[1].strip()

        a = ""
        if value[0] == '"':
            value = value[1]
        elif value.find("|") >= 0:
            value = [[int(i) for i in v.strip().split(" ")] for v in value.split("|")]
        else:
            # not a value or a |
            value = [int(i) for i in value.strip().split(" ")]
        rules[rule] = value
    elif mode == 1:
        messages.append(line)


print(rules)
print(messages)

def split_and_match(rule, content):
    #print("SnM", rule, content)
    if len(rule) == 1:
        return matches(rule[0], content)
    elif len(rule) == 2:
        #print("len 2", rule)
        for i in range(1, len(content) + 1):
            a = content[:i]
            b = content[i:]
            #print(a,b)
            if matches(rule[0], a) and matches(rule[1], b):
                return True
        return False
    elif len(rule) == 3:
        #print("len 3", rule)
        for i in range(1, len(content)):
            for j in range(i + 1, len(content) + 1):
                a = content[:i]
                b = content[i:j]
                c = content[j:]
                #print(a,b,c)
                if matches(rule[0],a) and matches(rule[1], b) and matches(rule[2], c):
                    return True
        return False
@lru_cache(maxsize=None)
def matches(rule_num, content):
    rule = rules[rule_num]
    #print("M", rule_num, rule, content)
    if type(rule) is str:
        return rule == content
    elif type(rule) is int:
        return matches(rule_num, content)
    elif type(rule) is list:
        #print(rule, "is list")
        if type(rule[0]) is list:
            #print(rule[0], "is list")
            return split_and_match(rule[0], content) or split_and_match(rule[1], content)
        elif type(rule[0]) is int:
            #print(rule[0], "is int")
            return split_and_match(rule, content)
        else:
            print("UNknown rule", rule)
    else:
        print("Unknown rule", rule)
    return False

num_matches = 0

for message in messages:
    print("*"*12)
    print(message)
    if matches(0, message):
        print("GOTEM", message)
        num_matches += 1

print(num_matches)


