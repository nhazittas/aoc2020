import sys
import re

pattern = re.compile("([0-9]*)\-([0-9]*) ([a-z]): ([a-z]*)")

valid = 0


def countLetter( password, char):
  count = 0
  for c in password:
    if c == char:
      count += 1
  return count

for line in sys.stdin:
  m = pattern.match(line)
  min = int(m.group(1))
  max = int(m.group(2))
  char = m.group(3)
  password = m.group(4)

  numChar = countLetter( password, char)
  if min <= numChar <= max:
    valid += 1

  #print(min, max, char, password, numChar, valid)

print(valid)
