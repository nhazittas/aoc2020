import sys
import copy
import math
import re
from collections import defaultdict
from functools import lru_cache
lines = [line.strip() for line in sys.stdin]

s = set()

p1 = []
p2 = []
p = 0

for line in lines:
    if len(line) == 0:
        continue
    if line.find("Player") >= 0:
        p += 1
        continue
    #print(line)
    n = int(line)
    if p == 1:
        p1.append(n)
    else:
        p2.append(n)

p1.reverse()
p2.reverse()

#print(p1)
#print(p2)

def play_round(p1l, p2l):
    #print("VAL",p1l,p2l)
    c1 = p1l.pop()
    c2 = p2l.pop()

    #print("ROUND",c1, c2)

    if len(p1l) >= c1 and len(p2l) >= c2:
        #print("   RECURSE")
        winner = play_recursive(p1l[-c1:].copy(), p2l[-c2:].copy())
        if winner == 1:
            #print("   P1 recurse win")
            p1l.insert(0,c1)
            p1l.insert(0,c2)
            return 1
        else:
            #print("   P2 recurse win")
            p2l.insert(0,c2)
            p2l.insert(0,c1)
            return 2
    elif c1 > c2:
        #print("P1 normie win")
        p1l.insert(0,c1)
        p1l.insert(0,c2)
        return 1
    elif c1 == c2:
        pass
        #print("EQUAL?")
    elif c1 < c2:
        #print("P2 normie win")
        p2l.insert(0,c2)
        p2l.insert(0,c1)
        return 2
game = 0

#@lru_cache(maxsize=None)
def play_recursive(l1, l2):
    global game
    game += 1 
    loc_game = game
    round = 0
    #print("PLAY RECURSE")
    seen_states = set()
    while len(l1) != 0 and len(l2) != 0:
        round +=1
        #print("G", loc_game, "R", round, l1, l2)
        state = (tuple(l1), tuple(l2))
        if state in seen_states:
            #print("STATE SEEN:P1 WIN", state)
            return 1
        seen_states.add(state)
        play_round(l1,l2)
    if len(l1) == 0:
        winner = p2
        return 2
    else:
        winner = p1
        return 1
    


winner = play_recursive(p1, p2)
if winner == 1:
    winner = p1
    print("P1 WIN")
else:
    winner = p2
    print("P2 WIN")
print(p1)
print(p2)

i = 0
s = 0
for card in winner:
    i+=1
    s += i*card
    print(s)


