import sys
import copy
import math
import re
from collections import defaultdict
from functools import lru_cache
lines = [line.strip() for line in sys.stdin]

s = set()

p1 = []
p2 = []
p = 0

for line in lines:
    if len(line) == 0:
        continue
    if line.find("Player") >= 0:
        p += 1
        continue
    print(line)
    n = int(line)
    if p == 1:
        p1.append(n)
    else:
        p2.append(n)

p1.reverse()
p2.reverse()

print(p1)
print(p2)

def play_round(p1, p2):
    print("VAL",p1,p2)
    c1 = p1.pop()
    c2 = p2.pop()

    print("ROUND",c1, c2)

    if c1 > c2:
        p1.insert(0,c1)
        p1.insert(0,c2)
        pass
    elif c1 == c2:
        print("EQUAL?")
    elif c1 < c2:
        p2.insert(0,c2)
        p2.insert(0,c1)

while len(p1) != 0 and len(p2) != 0:
    play_round(p1,p2)

if len(p1) == 0:
    winner = p2
else:
    winner = p1

i = 0
s = 0
for card in winner:
    i+=1
    s += i*card
    print(s)


