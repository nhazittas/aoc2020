import sys
import re

valid = 0

passports = []
p = dict()

groupSet = None
numYes = 0
i=0
for line in sys.stdin:
  line = line.strip()
  print(i, line)
  i+=1

  if len(line) == 0:
    numYes += len(groupSet)
    print(groupSet, len(groupSet), numYes)
    groupSet = None
    continue
  mySet = set()
  for c in line:
    mySet.add(c)
  if groupSet is None:
    groupSet = mySet
  else:
    groupSet &= mySet
  print(' ', groupSet)

if groupSet is None:
  print(numYes)
else:
  numYes += len(groupSet)
  print(groupSet, len(groupSet), numYes)

