import sys
import re

valid = 0

passports = []
p = dict()


lines = [int(line.strip()) for line in sys.stdin]

window_size = len(lines)
i = 0

magic_number = 144381670

for i in range(window_size):
    for j in range(i+1, window_size):
        window = lines[i:j]
        if sum(window) == magic_number:
            print(window)
            print(min(window) + max(window))
