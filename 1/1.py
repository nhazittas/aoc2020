import sys

nums = [int(line) for line in sys.stdin]
n = len(nums)
# find pair that sums to 2020, retun the product
for i in range(n):
  ni = nums[i]
  for j in range(i+1,n):
    nj = nums[j]
    if ni + nj == 2020:
      print( i, j, ni, nj, ni*nj)
      exit(0)
