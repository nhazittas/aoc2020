import sys

nums = [int(line) for line in sys.stdin]
n = len(nums)
# find pair that sums to 2020, retun the product
for i in range(n):
  ni = nums[i]
  for j in range(i+1,n):
    nj = nums[j]
    for k in range(j+1,n):
      nk = nums[k]
      if ni + nj + nk == 2020:
        print( ni*nj*nk)
        exit(0)
