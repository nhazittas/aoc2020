import sys
import copy
import math
import re
from collections import defaultdict 
lines = [line.strip() for line in sys.stdin]

mask = "0"*36
p=re.compile("mem\[([0-9]+)\]")
memory = defaultdict(int)


def convert_to_bit_string(val):
    og=val
    ret_val = ''
    while len(ret_val) < 36:
        if val % 2 == 1:
            ret_val="1"+ret_val
        else:
            ret_val="0"+ret_val
        val = val//2
    print(og,"->",ret_val)
    return ret_val

def convert_to_number(val):
    og = val
    #val = list(val)
    n=0
    while len(val) > 0:
        n*=2
        top = val.pop(0)
        if top == "1":
            n+=1
    print(og,"->",n)
    return n


def compute_val(val, mask, current_val):
    val = convert_to_bit_string(val)
    n = [ a if b == "X" else b for a,b in zip(val, mask)]
    return convert_to_number(n)
    

for line in lines:
    line = line.split(" = ")
    add = line[0]
    val = line[1]

    if add == 'mask':
        print('M =', val)
        mask = val
    else:
        print(add)
        address = int(p.match(add).group(1))
        memory[address] = compute_val(int(val), mask, memory[address])
        print("@0x",address, '=', memory[address])
print(memory)
s = sum([memory[i] for i in memory])
print(s)





    
    


