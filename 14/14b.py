import sys
import copy
import math
import re
from collections import defaultdict 
lines = [line.strip() for line in sys.stdin]

mask = "0"*36
p=re.compile("mem\[([0-9]+)\]")
memory = defaultdict(int)


def convert_to_bit_string(val):
    og=val
    ret_val = ''
    while len(ret_val) < 36:
        if val % 2 == 1:
            ret_val="1"+ret_val
        else:
            ret_val="0"+ret_val
        val = val//2
    print(og,"->",ret_val)
    return ret_val

def convert_to_number(val):
    og = val
    #val = list(val)
    n=0
    while len(val) > 0:
        n*=2
        top = val.pop(0)
        if top == "1":
            n+=1
    print(og,"->",n)
    return n


def compute_val(val, mask, current_val):
    val = convert_to_bit_string(val)
    n = [ a if b == "X" else b for a,b in zip(val, mask)]
    return convert_to_number(n)
    
def compute_addresses(address, mask):
    #print("compute_addresses")
    #print(address, mask)
    address = convert_to_bit_string(address)
    new_add = dict()
    for i in range(len(mask)):
        #print(mask[i])
        if mask[i] == '0':
            #print('set to', address[i])
            new_add[i] = [address[i]]
        elif mask[i] == '1':
            #print('set to 1')
            new_add[i] = ["1"]
        else:
            #print('set to 1/0')
            new_add[i] = ["0", "1"]

    #print("new_add")
    #print(new_add)
    
    add_list = [""]
    for i in range(len(mask)):
        print(i, mask[i], new_add[i])
        next_a_list = []
        choices = new_add[i]
        for a in add_list:
            for choice in choices:
                next_a_list.append(a+choice)
        add_list = next_a_list.copy()




    #print(address, mask, add_list)
    return add_list

for line in lines:
    line = line.split(" = ")
    add = line[0]
    val = line[1]

    if add == 'mask':
        print('M =', val)
        mask = val
    else:
        print(add)
        address = int(p.match(add).group(1))
        val = int(val)
        addresses = compute_addresses(address, mask)
        for a in addresses:
            memory[a] = val
            print("@0x",a, '=', memory[address])
print(memory)
s = sum([memory[i] for i in memory])
print(s)





    
    


