import sys
import copy
import math
import re
from collections import defaultdict 
lines = [line.strip() for line in sys.stdin]

brd = defaultdict(lambda : False)

for i in range(len(lines)):
    line = lines[i]
    for j in range(len(line)):
        char = line[j]
        if char == "#":
            brd[(i,j,0,0)] = True


def calculate_bounds(hood):
    mx,MX = 0,0
    my,MY = 0,0
    mz,MZ = 0,0
    mw,MW = 0,0

    for pt in hood:
        if hood[pt]:
            mx = min(mx, pt[0])
            MX = max(MX, pt[0])
            my = min(my, pt[1])
            MY = max(MY, pt[1])
            mz = min(mz, pt[2])
            MZ = max(MZ, pt[2])
            mw = min(mw, pt[3])
            MW = max(MW, pt[3])
    return ((mx,my,mz,mw),(MX,MY,MZ,MW))

def calc_new_itter_bounds(hood):
    m, M = calculate_bounds(hood)
    return ((m[0]-1, m[1]-1, m[2]-1,m[3]-1),(M[0]+1,M[1]+1,M[2]+1,M[3]+1))


def occupied_neighbors(hood, x, y, z,w):
    occupied = 0
    for dx in range(-1,2):
        row = x + dx
        for dy in range(-1,2):
            col = y + dy
            for dz in range(-1,2):
                hei = z + dz
                for dw in range(-1,2):
                    ww = w + dw
                    if dx == 0 and dy == 0 and dz ==0 and dw ==0:
                        continue
                    if hood[(row,col,hei,ww)]:
                        occupied += 1
    return occupied

def next_gen(hood):
    next_gen = defaultdict(lambda : False)
    m, M = calc_new_itter_bounds(hood)

    for x in range(m[0], M[0] + 1):
        for y in range(m[1], M[1] + 1):
            for z in range(m[2], M[2] + 1):
                for w in range(m[3], M[3] + 1):
                    og = hood[(x,y,z,w)]
                    n = occupied_neighbors(hood, x, y, z, w)
                    if (og and n == 2) or n == 3:
                        next_gen[(x,y,z,w)] = True
    return next_gen

def compare_seats(s1, s2):
    for row in range(1):
        for col in range(1):
            if s1[row][col] != s2[row][col]:
                return False
    return True

def print_2d(hood):
    print("-"*20)
    m, M = calculate_bounds(hood)
    for w in range(m[3], M[3] + 1):
        for z in range(m[2], M[2] + 1):
            print("z= ",z,", w=", w, sep="")
            for x in range(m[0], M[0] + 1):
                for y in range(m[1], M[1] + 1):
                    c = "#" if hood[(x,y,z,w)] else "."
                    print(c, end="")
                print()
            print()

def count(hood):
    s = 0
    for p in hood:
        if hood[p]:
            s +=1
    return s

print_2d(brd)
for i in range(6):
    print("round",1+i)
    brd = next_gen(brd)
    #print_2d(brd)

print(count(brd))


exit()
last_gen = lines
current_gen = next_gen(last_gen)

while not compare_seats(last_gen, current_gen):
    #print_2d(current_gen)
    last_gen = current_gen
    current_gen = next_gen(current_gen)

#print_2d(current_gen)
num_hash = sum([sum([1 if c == '#' else 0 for c in row]) for row in current_gen])
print(num_hash)
